// This file is part of Platformagent.
// 
// Platformagent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platformagent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platformagent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Platform "more stats" script
 *
 * @package     platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(['local_mooring/ajax'], function(ajax) {
    
    var viewModifier = {
        $setTable: function(data) {
            for(var type in data){
                var stat = data[type]
                var html = ''
                for(var row of stat){
                    console.log(row)
                    html += '<tr>'
                    
                    html += '<td>'+row.timestamp+'</td>'
                    html += '<td>'+row.all+'</td>'
                    
                    html += '<td>'
                    html += '<span class="stats-unique">'+row.unique+'</span><br/>'  
                    html += '<span class="stats-unique-students">'+row.unique_students+'</span>'
                    html += '&nbsp;|&nbsp;'
                    html += '<span class="stats-unique-teachers">'+row.unique_teachers+'</span>'
                    html += '</td>'
                    
                    html += '</tr>'
                }
                
                var $tbody = $('#platform-stats-'+type)
                $tbody.html(html)
            }
            if(iterations >= 30) $("#button-morestats").hide()
        },
    }
    
    var ajaxRequest = {
        
        moreStats: function(iterations) {
            return ajax.get('?way=stats.ajax.get',{iterations: iterations})
        },
        
    }
    
    var controller = {
        loading: function(i) {
            ajaxRequest.moreStats(i).then(function(ret) {
                ret = JSON.parse(ret)
                if(ret.data){
                    viewModifier.$setTable(ret.data)
                    iterations = Math.min(30,i + 5)   
                }
                $('button').prop('disabled', false)
            }).catch(function(error) {
                console.log(error)
                $('button').prop('disabled', false)
            })
        },      
    }
    
    var iterations = 12
    
    $('[data-action="morestats"]').click(function(e) {
        e.preventDefault()
        $('button').prop('disabled', true)
        controller.loading(iterations)
    })
    
})