# PLATFORMAGENT
## Never let a human to do a machine's job !

-----

Requirements
============

Needed Role
-----------

Before installing this plugin, you must create a new role called "platformmanager" :

        global $DB;
        if (!$DB->record_exists('role', ['shortname' => 'platformmanager'])) {
            $roleid = create_role("Gestionnaire platforme", 'platformmanager', "created by local_platformagent");
            assign_capability('local/platformagent:opendata', CAP_ALLOW, $roleid, context_system::instance());
            assign_capability('local/platformagent:school', CAP_ALLOW, $roleid, context_system::instance());
        }

You can run this code in db/install.php file for automation :

        function xmldb_local_platformagent_install() {
            // above code
        }

Installation
============

AMD Files
---------

After cloning or pulling the repository, you must minify js files in amd/src/ directory :

        moodle/local/mooring/amd$ grunt uglify