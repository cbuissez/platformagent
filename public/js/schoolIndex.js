// This file is part of Platformagent.
// 
// Platformagent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platformagent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platformagent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School index script
 *
 * @package     platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(['local_mooring/URI', 'local_mooring/ajax'], function(URI, ajax) {
    
    var viewModifier = {
        
        $adaptStepData: function(data) {
            if (typeof data.nature !== 'undefined')
                $('#form-data-nature').val(data.nature).prop('disabled', true)
            if (typeof data.name !== 'undefined' && data.name !== '')
                $('#form-data-name').val(data.name).prop('disabled', true)
            if (typeof data.city !== 'undefined' && data.city !== '')
                $('#form-data-city').val(data.city).prop('disabled', true)
        },
        
        $adaptStepReport: function(bool) {
            if (bool)
                $('#report-success').show()
            else
                $('#report-danger').show()
        },
        
        $modalLoadAddition: function(html) {
            var $modal = $('#modal-addition')
            $modal.html(html)

            $modal.on('shown.bs.modal', function () {
                $modal.find('#form-search-uai').focus()
            })
            
            $modal.on('hide.bs.modal', function () {
                document.location.href = document.location.href
            })

            $modal.find('button[data-addition="cancel"]').click(function() {
                controller.loading()
            })

            $modal.find('#form-search').submit(function(e) {
                e.preventDefault()
                $modal.find('#form-search-submit').prop('disabled', true)
                controller.searching($modal.find('#form-search-uai').val())
            })

            $modal.modal('show')

            $modal.find('#form-data').submit(function(e) {
                e.preventDefault()
                $modal.find('#form-data-submit').prop('disabled', true)
                controller.validation({
                    nature: $modal.find('#form-data-nature').val(),
                    name: $modal.find('#form-data-name').val(),
                    city: $modal.find('#form-data-city').val()
                })
            })

            $modal.find('#form-data-modify').click(function() {
                $modal.find('#form-data-nature').prop('disabled', false)
                $modal.find('#form-data-name').prop('disabled', false)
                $modal.find('#form-data-city').prop('disabled', false)
            })

        },
        
        $modalChangeStep: function(modal, step) {
            var selector = '#modal-' + modal
            var $steps = $(selector).find('div[data-step]')
            var $progress = $(selector).find('.progress-bar:first')
            $steps.each(function(index) {
                if ($(this).data('step') === step) {
                    var percentage = (index + 1) * 100 / $steps.length
                    $progress.css('width', percentage + '%')
                    $progress.text('Étape ' + (index + 1))
                    $(this).show()
                }
                else
                    $(this).hide()
            })
        },
        
        $modalAlert: function(modal, step, type, msg) {
            var selector = '#modal-' + modal + ' div[data-step="' + step + '"]'
            var $alert = $(selector).find('.alert:first')
            $alert.hide().removeClass('alert-success alert-warning alert-danger')
            $alert.addClass('alert-' + type).html(msg).show()
            $(selector).find('button[type="submit"]').prop('disabled', false)
        }
        
    }
    
    var school = {
        
        data: function() {
            return {
                uai: this.uai,
                nature: this.nature,
                name: this.name,
                city: this.city
            }
        },
        
        extend: function(data) {
            for (var datum in data) {
                this[datum] = data[datum]
            }
        },
        
        verifyUAI: function(uai) {
            return new Promise(function(resolve, reject) {
                var exp = /^([0-9]{7})([a-hj-npr-z]{1})$/i
                if (!exp.test(uai))
                    reject(Error("Ce code contient toujours 7 chiffres suivis d'une lettre !"))
                else {
                    // les lettres i, o et q ne sont pas utilisées
                    var CHARS = 'abcdefghjklmnprstuvwxyz'
                    var fragments = exp.exec(uai)
                    var modulo = fragments[1] % CHARS.length
                    if (CHARS.indexOf(fragments[2].toLowerCase()) !== modulo) {
                        reject(Error("La clé ne correspond pas, une erreur doit s'être glissée dans le code !"))
                    }
                    else {
                        resolve(uai)
                    }
                }
            })
        }
        
    }
    
    var ajaxRequest = {
        
        getModal: function(modal) {
            return ajax.get('?way=school.modal.' + modal)
        },
        
        schoolExist: function(uai) {
            return ajax.get('?way=school.ajax.exist', {
                uai: uai
            }).then(ajax.handle)
        },
        
        openDataQuery: function(uai) {
            return ajax.get('?way=opendata.ajax.query', {
                uai: uai,
                'fields[]': ['nature', 'name', 'city']
            }).then(ajax.handle)
        },
        
        schoolPost: function(data) {
            return ajax.post('?way=school.ajax.create', data).then(ajax.handle)
        }
        
    }
    
    var controller = {
        
        loading: function() {
            ajaxRequest.getModal('addition').then(function(html) {
                viewModifier.$modalLoadAddition(html)
            }).catch(function(error) {
                console.log(error)
            })
        },
        
        searching: function(uai) {
            school.verifyUAI(uai).then(function(uai) {
                return ajaxRequest.schoolExist(uai)
            }).then(function(exist) {
                if (exist)
                    throw new Error("Cet établissement existe déjà sur la plateforme !")
                school.uai = uai
                return ajaxRequest.openDataQuery(uai)
            }).then(function(data) {
                viewModifier.$adaptStepData(data)
                viewModifier.$modalChangeStep('addition', 'data')
            }).catch(function(error) {
                viewModifier.$modalAlert('addition', 'search', 'danger', error.message)
            })
        },
        
        validation: function(data) {
            school.extend(data)
            console.log(school.data())
            ajaxRequest.schoolPost(school.data()).then(function() {
                viewModifier.$adaptStepReport(true)
            }).catch(function(error) {
                console.log(error.message)
                viewModifier.$adaptStepReport(false)
            }).then(function() {
                viewModifier.$modalChangeStep('addition', 'report')
            })
        }
        
    }
    
    if (URI(document.location.href).hasQuery('modal', 'addition')) {
        controller.loading()
    }
    
    $('[data-action="addition"]').click(function(e) {
        e.preventDefault()
        controller.loading()
    })
    
})