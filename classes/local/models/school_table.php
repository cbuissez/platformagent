<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School table model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\school_table as school;
use local_mooring\local\library;
use local_mooring\local\config;
use local_platformagent\local\models\opendata_json;

class school_table extends school {
    
    public function all() {
        global $DB;
        $sql = "SELECT s.*
                  FROM {{$this->table}} s
            INNER JOIN {user} u
                    ON s.uai = u.username
              ORDER BY s.city, s.name";
        $schools = $DB->get_records_sql($sql);
        return $this->to_entities($schools);
    }
    
    public function one($uai) {
        global $DB;
        $sql = "SELECT s.*
                  FROM {{$this->table}} s
            INNER JOIN {user} u
                    ON s.uai = u.username
                 WHERE s.uai = :uai";
        $school = $DB->get_record_sql($sql, [
            'uai'   => $uai
        ]);
        if ($school) {
            return $this->to_entity($school);
        }
        return false;
    }
    
    public function exist($uai) {
        $school = $this->one($uai);
        if (isset($school->uai)) {
            return true;
        }
        return false;
    }
    
    /*public function cas($uai, $nature) {
        return config::load('cas')->is_any_cas($uai, $nature);
    }*/
    
    public function create($data) {
        global $DB, $USER;
        
        library::test_uai($data->uai);
        $data->uai = strtolower($data->uai);
        
        $data->dept = (int) substr($data->uai, 0, 3);
        if (!in_array($data->dept, config::load('base')->get('depts'))) {
            throw new \Exception("SCHOOL: out of project area");
        }
        if ($DB->get_record($this->table, array ('uai' => $data->uai), 'id')) {
            throw new \Exception("SCHOOL: already exists");
        }

        //On indique le CAS seulement si l'établissement est public
        $school_public = (new opendata_json())->query($data->uai,['statut_public_prive']);
        $public = strtolower($school_public->statut_public_prive) === "public";
        $data->cas = $public ? library::is_any_cas($data->uai, $data->nature) : null;

        $data->time = time();
        $data->who = $USER->username;
        
        $DB->insert_record($this->table, $data);
        
        return $data;
    }
    
}
