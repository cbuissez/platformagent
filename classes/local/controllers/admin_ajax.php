<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration ajax controller
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\controllers;

use local_mooring\local\controllers\app_controller;

class admin_ajax extends app_controller {
    
    public function __construct() {
        parent::__construct();
        require_capability('local/platformagent:admin', $this->context);
        $this->load_model('admin_table', 'admin');
    }
    
    public function update() {
        $this->load_model('admin_slack', 'slack');
        
        $data = (object) filter_input_array(INPUT_POST, [
            'table' => FILTER_SANITIZE_STRING,
            'id'    => FILTER_VALIDATE_INT,
            'field' => FILTER_SANITIZE_STRING,
            'old'   => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
            'new'   => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES)
        ]);
        
        $this->slack->available && $this->slack->update_attempt($data->table);
        $this->admin->update($data);
        $this->slack->available && $this->slack->update_table($data);
        
        return true;
    }
    
}
