<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Open data table model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\library;
use local_platformagent\local\models\opendata_entity;

class opendata_json {
    
    public function query($uai, $fields) {
        library::test_uai($uai);
        $entity = $this->get_school_entity($uai);
        if($entity){
            $data = new \stdClass();
            foreach ($fields as $field) {
                $data->$field = $entity->$field;
            }
            return $data;
        }
        return false;
    }
    
    private function get_school_entity($uai) {
        return $this->get_school_data($uai);
    }
    
    private function get_school_data($uai){
        //$url = 'https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-annuaire-education&facet=identifiant_de_l_etablissement&facet=nom_etablissement&facet=type_etablissement&facet=statut_public_prive&facet=code_postal&facet=nom_commune&facet=code_departement&facet=code_academie&facet=code_region&facet=ecole_maternelle&facet=ecole_elementaire&facet=voie_generale&facet=voie_technologique&facet=voie_professionnelle&facet=restauration&facet=hebergement&facet=ulis&facet=apprentissage&facet=segpa&facet=section_arts&facet=section_cinema&facet=section_sport&facet=section_internationale&facet=section_europeenne&facet=lycee_agricole&facet=lycee_militaire&facet=lycee_des_metiers&facet=post_bac&facet=appartenance_education_prioritaire&facet=nombre_d_eleves';
        $url = 'https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-annuaire-education&facet=identifiant_de_l_etablissement&facet=nom_etablissement&facet=type_etablissement&facet=nom_commune';
        $url .= '&refine.identifiant_de_l_etablissement='.strtoupper($uai);
        $file = fopen($url, "r");
        if (!$file) return false;
        $object = json_decode(fgets($file));
        if($object && isset($object->records) && isset($object->records[0]) && isset($object->records[0]->fields)){
            $result = $object->records[0]->fields;
            return new opendata_entity((object) $result);
        }
        return false;
    }
}
