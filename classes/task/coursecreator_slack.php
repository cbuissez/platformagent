<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/01/19
 * Time: 09:51
 */

namespace local_platformagent\task;


use local_mooring\local\models\app_slack;

class coursecreator_slack extends app_slack
{
    private $relevant;

    private $nbeditingteachers;
    private $suggestedteacher;

    private $firsteditor;

    public function __construct($courseid) {
        parent::__construct();

        $this->relevant = false;
        $this->nbeditingteachers = -1;
        $this->firsteditor = null;


        global $CFG;
        $server = $CFG->wwwroot;

        $this->text(" modifie les rôles du cours [$courseid]($server/user/index.php?id=$courseid)");
    }

    /**
     * Construct message about editing teachers : nb of editing teachers
     * @param $nbeditingteachers
     */
    public function nb_editing_teachers($nbeditingteachers) {
        $this->nbeditingteachers = $nbeditingteachers;
    }

    /**
     * Warn that a teacher, as a unique editor, is set to creator
     * @param $userid
     */
    public function unique_editing_teacher($userid) {
        $this->suggestedteacher = $userid;
        $this->relevant = true;
    }

    /**
     * Finish constructing the message about teachers
     */
    public function editing_teacher_check_finish() {
        switch ($this->nbeditingteachers) {
            case 0:
                $this->attachment([
                    'color' => 'good',
                    'title' => "Pas d'enseignant (éditeur) pour ce cours."]);
                break;
            case 1:
                $this->attachment([
                    'color' => 'warning',
                    'title' => "Un unique enseignant éditeur, l'utilisateur $this->suggestedteacher 
                        a été trouvé. Il est maintenant considéré comme le créateur"]);
                break;
            default:
                $this->attachment([
                    'color' => 'good',
                    'title' => "Plusieurs enseignants (éditeur) trouvés pour ce cours."]);
                break;
        }
    }

    /**
     * Construct a message about creator found in logs
     * @param $user
     * @param $deleted
     */
    public function creator_in_logs($user, $deleted) {
        if ($user) {
            if ($deleted) {
                $this->attachment([
                    'color' => 'warning',
                    'title' => "Le créateur du cours a été trouvé dans les logs ($user->userid) mais il a été supprimé."
                ]);
            } else {
                $this->attachment([
                    'color' => 'warning',
                    'title' => "Le créateur du cours a été trouvé dans les logs et a maintenant retrouvé son rôle : utilisateur $user->userid"
                ]);
            }
        } else {
            $this->attachment([
                'color' => 'warning',
                'title' => 'Le créateur réel n\'a pas été retrouvé dans les logs !'
            ]);
        }

        $this->relevant = true;
    }

    /**
     * @param $userlist : list of tables of userid with a relevant score
     */
    public function list_potential_creators($userlist, $criteria) {
        if ($userlist) {
            if (count($userlist) === 0) {
                $this->attachment([
                    'color' => 'warning',
                    'title' => "Aucun d'enseignant valide ayant édité ce cours retrouvé. Il faudrait rattacher ce cours au compte administrateur."
                ]);
            } else {
                $this->array_to_fields($userlist)->attachment([
                    'color' => 'warning',
                    'title' => "Liste des utilisateurs candidats à être créateur du cours selon $criteria"
                ]);
            }

            $this->relevant = true;
        }
    }

    public function error_for_course($message) {
        $this->attachment([
            'color' => 'danger',
            'title' => $message
        ]);
        $this->send();
    }

    public function send_if_relevant() {
        if ($this->relevant) {
            $this->send();
        }

        return $this->relevant;
    }

}