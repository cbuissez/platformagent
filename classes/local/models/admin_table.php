<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration table model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\core_table;

class admin_table extends core_table {
    
    public function select($table) {
        global $DB;
        return $DB->get_records($table);
    }
    
    public function update($data) {
        global $DB;
        if ($data->new === '') {
            $data->new = null;
        }
        $object = (object) ['id' => $data->id, $data->field => $data->new];
        return $DB->update_record($data->table, $object);
    }
    
    public function insert($table, $data) {
        global $DB;
        return $DB->insert_record($table, $data, false);
    }
    
    public function delete($table, $id) {
        global $DB;
        return $DB->delete_records($table, ['id' => $id]);
    }
    
}
