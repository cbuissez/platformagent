<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration table view
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $PAGE;
$PAGE->requires->js(new moodle_url('/local/platformagent/public/js/adminTable.js'));

?>

<div class="modal fade" id="modal-modification"></div>

<h2>Données brutes de la table <span id="table-name"><?php echo $table ?></span></h2>

<?php if (count($data)) : ?>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <?php foreach (reset($data) as $key => $row): ?>
                <th class="text-center"><?php echo strtoupper($key) ?></th>
                <?php endforeach ?>
            </tr>
        </thead>
        <tbody id="table-data">
            <?php foreach ($data as $tr): ?>
            <tr>
                <?php foreach ($tr as $field => $td): ?>
                <td class="text-center" data-field="<?php echo $field ?>"><span data-placement="right" title=""><?php echo $td ?></span></td>
                <?php endforeach ?>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<?php else : ?>
<div class="alert alert-warning">La table ne contient pas le moindre enregistrement !</div>
<?php endif ?>

<!--<button type="button" class="btn-primary center-block">Ajouter une nouvelle entrée</button>-->