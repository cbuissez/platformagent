<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School index view
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $PAGE;
$PAGE->requires->js(new moodle_url('/local/platformagent/public/js/schoolIndex.js'));
$PAGE->requires->js(new moodle_url('/local/platformagent/public/js/schoolStats.js'));

?>

<div class="modal fade" id="modal-addition"></div>
<div class="modal fade" id="modal-stats"></div>

<header class="local">
    <div class="title"><h2>Gestion des établissements</h2></div>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="#" data-action="addition">
                    <span class="glyphicon glyphicon-plus-sign">&nbsp;</span>Nouvel établissement
                </a>
            </li>
            <li>
                <a href="<?php echo new moodle_url('/local/platformagent/index.php?way=stats.page.index') ?>">
                    <span class="glyphicon glyphicon-stats">&nbsp;</span>Statistiques du bassin
                </a>
            </li>
        </ul>
        <!--<form class="navbar-form">
            <div class="input-group">
                <label class="input-group-addon" for="search"><span class="glyphicon glyphicon-search"></span></label>
                <input type="search" placeholder="Chercher un établissement" class="form-control" id="search">
            </div>
        </form>-->
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="mailto:support-elea@ac-versailles.fr">
                    <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                    <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?php echo new moodle_url('/local/faq/index.php?role=cb') ?>" target="_blank">
                    <span class="glyphicon glyphicon-info-sign">&nbsp;</span>Accéder au tutoriel
                </a>
            </li>
        </ul>
    </nav>
</header>

<?php if (count($schools)) : ?>
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Établissement</th>
                <th>Ville</th>
                <th>UAI</th>
                <th>CAS</th>
                <?php if (reset($schools)->studentslastimport !== null) : ?>
                <th>Import élèves</th>
                <?php endif ?>
                <th></th>
                <!--<th>Délégation</th>-->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($schools as $school): ?>
            <tr data-action="stats" data-uai="<?php echo $school->uai ?>">
                <td><?php echo $school->name ?></td>
                <td><?php echo $school->city ?></td>
                <td class="text-center"><?php echo strtoupper($school->uai) ?></td>
                <td class="text-center"><?php echo $school->casname ?></td>
                <?php if (reset($schools)->studentslastimport !== null) : ?>
                <td class="text-center <?php echo $school->studentslastimport->class ?>">
                    <?php echo ucfirst($school->studentslastimport->timeago) ?>
                </td>
                <?php endif ?>
                <td><span class="glyphicon glyphicon-new-window"></span></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<?php else : ?>
<div class="alert alert-warning">Aucun établissement sur cette plateforme pour l'instant !</div>
<?php endif ?>

<div class="form-group text-center">
     <button type="button" class="btn-default" data-action="addition">Ajouter un nouvel établissement</button>
</div>