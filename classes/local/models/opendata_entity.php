<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Open data entity model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\core_entity;

class opendata_entity extends core_entity {
    
    private $school;
    
    public function __construct($object) {
        $this->school = $object;
    }
    
    public function get_nature() {
        $nature = (string) $this->school->type_etablissement;
        $nature = strtolower($nature);
        if(strpos($nature,"ecole") !== false || strpos($nature,"école") !== false) return "ecole";
        if(strpos($nature,"lyc") !== false) return "lycee";
        if(strpos($nature,"coll") !== false) return "college";
        return null;
    }
    
    public function get_name() {
        $name = (string) $this->school->nom_etablissement;
        if (strlen($name) > 100 && strpos($name, ',') !== false) {
            $name = strstr($name, ',', true);
        }
        if (strlen($name) > 100) {
            $name = '';
        }
        return trim($name);
    }
    
    public function get_city() {
        return (string) $this->school->nom_commune;
    }

    public function get_statut_public_prive(){
        return strtolower($this->school->statut_public_prive);
    }
    
}
