<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English language file
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = "Platform Agent";

$string['platformagent:admin'] = "Modifier les tables des plugins locaux Mooring et Massimilate";
$string['platformagent:opendata'] = "Accéder aux données publiques des établissements scolaires";
$string['platformagent:school'] = "Gérer les établissements de la platforme";

$string['title'] = "Platform Agent";
$string['heading'] = "Platform Agent";

$string['schoolmailnewmanagersubject'] = "ELEA - Accès à votre compte de gestion sur la plateforme";
$string['schoolmailnewmanagermessage'] = 'À l\'attention de l\'Inspecteur de circonscription / du Chef d\'établissement
de l\'école / de l\'établissement {$a->school}
de {$a->city}

Madame, Monsieur,

Vous disposez désormais d\'un compte de gestion sur la plateforme
Éléa dont vous avez la responsabilité. Il permet d\'activer
le service Éléa pour votre école / établissement.

Vous pouvez déléguer la procédure qui suit
au référent Éléa que vous aurez désigné.

********************************************************

Pour activer le compte de gestion : 

- Commencez par cliquer sur le lien suivant :
{$a->link}
  Définissez votre mot de passe.
  Pour des raisons de sécurité, ce lien n\'est valable qu\'un mois. 

- Pour accéder à votre compte de gestion, connectez-vous à la plateforme :
{$a->wwwroot}
  Cliquez sur "Démarrer", puis "Utiliser mon compte local".
  Le nom d\'utilisateur correspond à l\'UAI de votre établissement
  en minuscule ({$a->username}).

********************************************************

Pour créer les comptes des utilisateurs, le référent Éléa de votre
établissement est invité à rejoindre le groupe des référents Éléa sur
Viaeduc : http://www.viaeduc.fr/group/10245. Il aura la possibilité d\'y
poser des questions et de consulter la documentation.

Votre conseiller de bassin {$a->cbfirstname} {$a->cblastname}
reste à votre disposition pour toute information complémentaire :
{$a->cbemail}

-- 

L\'équipe e-éducation 
Délégation académique au numérique éducatif 
Académie de Versailles';
$string['coursecreatortask'] = 'Vérification de la présence d\'un course creator pour chaque parcours';