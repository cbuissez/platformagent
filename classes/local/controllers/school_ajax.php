<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School ajax controller
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\controllers;

use local_mooring\local\controllers\app_controller;

class school_ajax extends app_controller {
    
    public function __construct() {
        parent::__construct();
        require_capability('local/platformagent:school', $this->context);
        $this->load_model('school_table', 'school');
    }
    
    public function exist() {
        $uai = filter_input(INPUT_GET, 'uai', FILTER_SANITIZE_STRING);
        return $this->school->exist($uai);
    }
         
    public function create() {
        $this->load_model('manager_user', 'manager');
        $this->load_model('school_slack', 'slack');
        $this->load_model('school_mail', 'mail');
        
        $data = (object) filter_input_array(INPUT_POST, [
            'uai'       => FILTER_SANITIZE_STRING,
            'nature'    => FILTER_SANITIZE_STRING,
            'name'      => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
            'city'      => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES)
        ]);
        
        $this->slack->available && $this->slack->create_attempt($data->uai);
        $school = $this->school->create($data);
        $this->slack->available && $this->slack->create_school($school);
        $manager = $this->manager->school_create($data);
        $this->slack->available && $this->slack->create_manager($manager);
        $this->mail->new_manager($manager, $school);
        $this->slack->available && $this->slack->create_mail();
        
        return true;
    }
    
}
