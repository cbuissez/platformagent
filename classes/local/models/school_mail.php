<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School mail model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\core_mail;

class school_mail extends core_mail {
    
    public function new_manager($user, $school) {
        global $CFG, $DB, $USER;
        
        $data = new \stdClass();
        $data->school = $school->name;
        $data->city = $school->city;
        $data->wwwroot = $CFG->wwwroot;
        $data->username = $user->username;
        
        if (strpos($USER->username, 'manager') === false) {
            $manager = $DB->get_record('user', ['username' => 'manager'], 'firstname, lastname, email', MUST_EXIST);
        } else {
            $manager = $USER;
        }
        
        $data->cbfirstname = $manager->firstname;
        $data->cblastname = $manager->lastname;
        $data->cbemail = $manager->email;
        
        $data->token = $this->create_reset_token($user->id);
        $data->link = $CFG->httpswwwroot . '/login/forgot_password.php?token=' . $data->token;
        
        $subject = get_string('schoolmailnewmanagersubject', 'local_platformagent');
        $message = get_string('schoolmailnewmanagermessage', 'local_platformagent', $data);
        
        $this->send($user->id, 2, $subject, $message);
        
        return $data;
    }
    
    private function create_reset_token($userid) {
        global $DB;
        $reset = new \stdClass();
        $reset->timerequested = time() + (30 * 24 * 60 * 60);
        $reset->userid = $userid;
        $reset->token = random_string(32);
        $DB->insert_record('user_password_resets', $reset);
        return $reset->token;
    }
    
}
