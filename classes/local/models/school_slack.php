<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School slack model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\app_slack;

class school_slack extends app_slack {
    
    public function create_attempt($uai) {
        $uai = strtoupper($uai);
        $server = filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRING);
        $this->text(" a tenté d'ajouter l'établissement $uai sur la [plateforme]($server)...");
    }
    
    public function create_school($school) {
        $this->field([
            'title' =>  "Nom",
            'value' =>  $school->name
        ])->field([
            'title' =>  "UAI",
            'value' =>  $school->uai
        ])->field([
            'title' =>  "Ville",
            'value' =>  $school->city
        ])->field([
            'title' =>  "CAS",
            'value' =>  (string) $school->cas
        ])->attachment([
            'color' =>  'good',
            'title' =>  "L'établissement suivant a été ajouté :"
        ]);
    }
    
    public function create_manager($manager) {
        $this->field([
            'title' =>  "Nom d'utilisateur",
            'value' =>  $manager->username
        ])->field([
            'title' =>  "Adresse mail",
            'value' =>  $manager->email
        ])->attachment([
            'color' =>  'good',
            'title' =>  "Son compte de gestion vient d'être créé :"
        ]);
    }
    
    public function create_mail() {
        $this->attachment([
            'color' =>  'good',
            'title' =>  "Un mail a été transmis au Chef d'établissement pour l'initialisation du mot de passe !"
        ])->send();
    }
    
}
