<?php

/**
 * Task scheduled by CRON:
 *  check if a course has been set with no coursecreator.
 * If so, retrieve course creator among editing teacher or former course creator
 *
 * @package     Platformagent
 * @author      Nicolas Daugas
 * @copyright   2018 Académie de Versailles
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\task;

use core\message\message;
use core\plugininfo\enrol;
use core\task\scheduled_task;
use core_calendar\local\event\forms\update;
use core_user\search\user;
use function Sodium\add;

class userinlog {

    public $id;
    public $time;

    public function __construct($id, $time)
    {
        $this->id = $id;
        $this->time = $time;
    }
}

class coursecreator_task extends scheduled_task {

    const COLOR_INFO    = "\033[0;36m";
    const COLOR_SUCCESS = "\033[0;32m";
    const COLOR_WARNING = "\033[0;33m";
    const COLOR_DANGER  = "\033[0;31m";
    const COLOR_DEFAULT = "\033[m";

    // IDs
    private $coursecreatorid;
    private $managerid;
    private $editingteacherid;
    private $teacherid;

    // Message sent
    private $slack;

    private $scorelist;
    const MANAGER_POINTS = 0;
    const EDITING_TEACHER_POINTS = 0;
    const TEACHER_POINTS = 0;
    const CREATOR_IN_LOG = 0;
    const POINTS_BY_MODIFICATION = 1;

    private $firstmodificationtab;

    /**
     * @var Moodle object used to get access to enrol plugins per course
     */
    private $enrolmanager;

    /**
     * @var Moodle object related to course to modify enrolment
     */
    private $enrolplugin;


    public function __construct()
    {
        global $DB;
        // Get static IDs
        $this->coursecreatorid = $DB->get_record('role', ['shortname' => 'coursecreator'], 'id', MUST_EXIST)->id;
        $this->managerid = $DB->get_record('role', ['shortname' => 'manager'], 'id', MUST_EXIST)->id;
        $this->editingteacherid = $DB->get_record('role', ['shortname' => 'editingteacher'], 'id', MUST_EXIST)->id;
        $this->teacherid = $DB->get_record('role', ['shortname' => 'teacher'], 'id', MUST_EXIST)->id;

        $this->enrolmanager = enrol_get_plugin('manual');
    }


    public function get_name()
    {
        return get_string('coursecreatortask', 'local_platformagent');
    }

    public function execute()
    {
        global $DB;
        $this->ctrace('**********************************', self::COLOR_SUCCESS);
        $this->ctrace("Course creator role id = ". $this->coursecreatorid . ", editing teacher role id = " . $this->editingteacherid . ", manager role id = " . $this->managerid, self::COLOR_INFO);

        // Find courses whithout coursecreator in a single DB request
        $courses = $DB->get_records_sql('
                SELECT c.id 
                FROM {course} AS c 
                LEFT JOIN {context} AS cont 
                ON contextlevel = :coursecontext AND instanceid = c.id 
                LEFT JOIN {role_assignments} 
                ON contextid = cont.id AND roleid = :coursecreatorroleid 
                WHERE roleid IS NULL',
            [
                'coursecontext' => CONTEXT_COURSE,
                'coursecreatorroleid' => $this->coursecreatorid
            ]
        );

        $this->ctrace("Courses without coursecreator: " . (count($courses)-1), self::COLOR_INFO);

        foreach ($courses as $course) {
            if ($course->id != 1) {// no process for course of id 1 (main page of moodle)

                $contextid = \context_course::instance($course->id, MUST_EXIST)->id;
                $hasnocreator = true;
                $this->ctrace("\tCourse $course->id (context $contextid) has no course creator!", self::COLOR_WARNING);

                $this->slack = new coursecreator_slack($course->id);
                $this->scorelist = [];
                $this->firstmodificationtab = [];

                $enrolinstances = enrol_get_instances($course->id, true);
                $this->enrolplugin = null;
                foreach ($enrolinstances as $courseenrolinstance) {
                    if ($courseenrolinstance->enrol == "manual") {
                        $this->enrolplugin = $courseenrolinstance;
                        break;
                    }
                }
                if (!$this->enrolplugin) {
                    $this->slack->error_for_course("Le plugin d'enrollement manuel n'a pas été trouvé pour ce parcours");
                    continue;
                }

                // If course hasn't found a course creator, check if there is a unique one
                if ($hasnocreator) {
                    $this->ctrace("\t\tchecking among editing teachers", self::COLOR_INFO);

                    $editingteachers = $DB->get_records('role_assignments', ['contextid' => $contextid, 'roleid' => $this->editingteacherid]);
                    foreach ($editingteachers as $editingteacher) {
                        $this->update_score_list($editingteacher->userid, self::EDITING_TEACHER_POINTS);
                    }

                    $this->ctrace("\t\tnb editing teachers = " . count($editingteachers), self::COLOR_INFO);
                    $this->slack->nb_editing_teachers(count($editingteachers));
                    if (count($editingteachers) === 0) {
                        $this->ctrace("\t\tNo editing teacher!", self::COLOR_WARNING);
                    } else if (count($editingteachers) === 1) {
                        $this->ctrace("\t\tFound a unique editing teacher => set course creator.", self::COLOR_INFO);
                        $teacher =  array_values($editingteachers)[0];
                        $userid = $teacher->userid;

                        $this->addrole($this->coursecreatorid, $contextid, $userid);

                        $this->slack->unique_editing_teacher($userid);

                        $hasnocreator = false;
                    }

                    $this->slack->editing_teacher_check_finish();
                }

                // If course creator hasn't still be found amonst teacher, search in logs when it was created
                if ($hasnocreator) {
                    $realcreator = $DB->get_record('logstore_standard_log',
                        ['eventname' => '\core\event\course_created',
                        'contextid' => $contextid],
                        'userid');

                    // May have beed deleted from the log
                    if ($realcreator) {
                        $deleted = $this->user_deleted($realcreator->userid);
                        $this->ctrace("\t\tFound creator (user " . $realcreator->userid . ") in logs", self::COLOR_INFO);
                        // Check that user is still in DB
                        if (!$deleted) {
                            // Check if he is an editing teacher :
                            $iseditingteacher = $DB->get_record('role_assignments',
                                ['contextid' => $contextid, 'roleid' => $this->editingteacherid, 'userid' => $realcreator->userid]);
                            if ($iseditingteacher === false) {
                                $this->addrole($this->editingteacherid, $contextid, $realcreator->userid);
                            }

                            $this->ctrace("\t\t=> will be the new coursecreator!", self::COLOR_INFO);
                            $this->addrole($this->coursecreatorid, $contextid, $realcreator->userid);

                            $this->update_score_list($realcreator->userid, self::CREATOR_IN_LOG);

                            $hasnocreator = false;
                        } else {
                            $this->ctrace("\t\t... but it seems he has left us !", self::COLOR_WARNING);
                        }
                    }

                    $this->slack->creator_in_logs($realcreator, $deleted);

                }

                // If creator not found in logs, find the first one still existing that has modified
                if ($hasnocreator) {

                    // Searching course changes
                    $evList = $DB->get_records('logstore_standard_log',
                        ['eventname' => '\core\event\course_updated',
                            'contextid' => $contextid],
                        'timecreated ASC');
                    $firstuser = $this->get_first_valid_user($evList);
                    $this->update_score_with_event_list($evList);

                    // Searching modules changes
                    $modules = $DB->get_records('course_modules', ['course' => $course->id]);
                    foreach ($modules as $module) {
                        $contextmodule = \context_module::instance($module->id, MUST_EXIST)->id;

                        // Module creation
                        $evList = $DB->get_records('logstore_standard_log',
                            ['eventname' => '\core\event\course_module_created',
                                'contextid' => $contextmodule],
                            'timecreated ASC');
                        $firstuser = $this->first_user($firstuser, $this->get_first_valid_user($evList));
                        $this->update_score_with_event_list($evList);

                        // Module changes
                        $evList = $DB->get_records('logstore_standard_log',
                            ['eventname' => '\core\event\course_module_updated',
                                'contextid' => $contextmodule],
                            'timecreated ASC');
                        $firstuser = $this->first_user($firstuser, $this->get_first_valid_user($evList));
                        $this->update_score_with_event_list($evList);

                        // Mo
                    }

                    // Searching section changes
                    $sections = $DB->get_records('course_sections', ['course' => $course->id]);
                    foreach ($sections as $section) {
                        // Section creation
                        $evList = $DB->get_records('logstore_standard_log',
                            ['eventname' => '\core\event\course_section_created',
                                'contextid' => $contextid],
                            'timecreated ASC');
                        $firstuser = $this->first_user($firstuser, $this->get_first_valid_user($evList));
                        $this->update_score_with_event_list($evList);

                        // Section changes
                        $evList = $DB->get_records('logstore_standard_log',
                            ['eventname' => '\core\event\course_section_updated',
                                'contextid' => $contextid],
                            'timecreated ASC');
                        $firstuser = $this->first_user($firstuser, $this->get_first_valid_user($evList));
                        $this->update_score_with_event_list($evList);
                    }

                    // Deletion of module or section
                    $evList = $DB->get_records_select('logstore_standard_log',
                        'courseid=' . $course->id .
                        ' and action="deleted" and (target="course_module" or target="course_section")');
                    $this->update_score_with_event_list($evList);

                    if ($firstuser) {
                        $this->ctrace("\t\tFound a user (" . $firstuser->id . ") who has edited this course in logs");
                        // Check that user is still in DB
                        $this->ctrace("\t\t=> may be chosen to be the new coursecreator!");
                    }

                    $this->ctrace("\t\tNo course creator found!", self::COLOR_WARNING);

                    $teachers = $DB->get_records('role_assignments', ['contextid' => $contextid, 'roleid' => $this->teacherid]);
                    foreach ($teachers as $teacher) {
                        $this->update_score_list($teacher->userid, self::TEACHER_POINTS);
                    }

                    arsort($this->scorelist);
                    $this->slack->list_potential_creators($this->scorelist, 'le nombre de modifications');
                    asort($this->firstmodificationtab);
                    $this->slack->list_potential_creators($this->firstmodificationtab, 'la date de la première modification');
                }

                if ($this->slack->send_if_relevant()) {
                    $this->ctrace("\t\tsending message", self::COLOR_INFO);
                }
            }
        }
        $this->ctrace('**********************************', self::COLOR_SUCCESS);
    }

    /**
     * Colored trace
     * @param $msg
     * @param string $color
     */
    private function ctrace($msg, $color = self::COLOR_DEFAULT)
    {
        mtrace($color . $msg . self::COLOR_DEFAULT);
    }

    /**
     * Add role in mdl_role_assignments for specific user and context
     * @param $roleid
     * @param $contextid
     * @param $userid
     * @throws \dml_exception
     */
    private function addrole($roleid, $contextid, $userid) {
        $this->enrolmanager->enrol_user($this->enrolplugin, $userid, $roleid);
        $this->ctrace("\t\t\tadding role " . $roleid . " for user " . $userid, self::COLOR_INFO);


    }

    /**
     * Check in DB if user exist and is not in deleted status
     * @param $userid
     */
    private function user_deleted($userid) {
        global $DB;
        $user = $DB->get_record('user', ['id' => $userid], 'id, deleted');
        if ($user) {
            return $user->deleted;
        }
        return true;
    }

    /**
     * get first user who is not deleted from an event list supposed to be sorted by time descending
     *
     * @param $eventlist
     * @return userinlog|null
     * @throws \dml_exception
     */
    private function get_first_valid_user($eventlist) {
        global $DB;

        $firstUser = null;
        foreach ($eventlist as $event) {
            if (!$this->user_deleted($event->userid)) {
                $firstUser = new userinlog($event->userid, $event->timecreated);
                break;
            }
        }

        return $firstUser;
    }

    /**
     * From two users, pick the first one to have made changes
     * @param userinlog $user1
     * @param userinlog $user2
     * @return userinlog
     */
    private function first_user($user1, $user2) {
        if ($user1) {
            if ($user2) {
                if ($user1->time > $user2->time) {
                    return $user2;
                }
                return $user1;
            }
            return $user1;
        }
        return $user2;
    }

    /**
     * Update table of scores for each users, candidates for being a course creator
     * @param $userid
     * @param $scoretoadd
     */
    private function update_score_list($userid, $scoretoadd, $time = INF) {
        if (!$this->user_deleted($userid)) {
            if (isset($this->scorelist[$userid])) {
                $this->scorelist[$userid] += $scoretoadd;
                if ($time < $this->firstmodificationtab[$userid]) {
                    $this->firstmodificationtab[$userid] = $time;
                }
            } else {
                $this->scorelist[$userid] = $scoretoadd;
                $this->firstmodificationtab[$userid] = $time;
            }
        }
    }

    private function update_score_with_event_list($eventlist) {
        foreach ($eventlist as $event) {
            $this->update_score_list($event->userid, self::POINTS_BY_MODIFICATION, $event->timecreated);
        }
    }
}
