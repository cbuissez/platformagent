<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School modal controller
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\controllers;

use local_mooring\local\controllers\app_controller;
use local_mooring\local\config;

class school_modal extends app_controller {
    
    public function __construct() {
        parent::__construct();
        require_capability('local/platformagent:school', $this->context);
    }
    
    public function addition() {
        $this->render('school.addition');
    }
    
    public function stats() {
        $uai = filter_input(INPUT_GET, 'uai', FILTER_SANITIZE_STRING);
        
        //Statistiques d'utilisation
        $field_profil = config::load()->get_user_field_id('profil');
        $field_uai = config::load()->get_user_field_id('uai');
        $stats_table = new \local_mooring\local\models\stats_table($field_profil,$field_uai);
        $school_stats = $stats_table->get_login_array_by_school($uai,time(),12,true);
        $school_other = $stats_table->get_other_stats_by_school($uai);

        //Derniers imports de chaque type
        $imports = [];
        $import_table_model = 'local_massimilate\local\models\import_table';
        if (class_exists($import_table_model)) {
            $import = new $import_table_model();
            $this->load_model('school_table', 'school');
            $school = $this->school->one($uai);
            $imports = $import->status_all($school);
        }
        
        $this->render('school.stats', compact('school_stats','uai','imports','school_other'));
    }
    
}
